## Overview

This issue represents the release planning for the [Gitaly Group](https://about.gitlab.com/handbook/product/categories/#gitaly-group) for the milestone listed in the above title.

## Problem Validation / Solution Validation

## Features

## Performance / Bug Fixes / Engineering Backlog

## Verification Efforts - @john.mcdonnell

## References
[Gitaly Kickoff Video]()

/cc @eread
/assign @mjwood @andrashorvath @john.mcdonnell
/label ~"group::gitaly" ~"Planning Issue" ~"Category:Gitaly" ~"section::enablement" ~"devops::systems" 
